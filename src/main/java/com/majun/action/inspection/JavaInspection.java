//package com.majun.action.inspection;
//
//import com.intellij.codeInspection.AbstractBaseJavaLocalInspectionTool;
//import com.intellij.codeInspection.ProblemsHolder;
//import com.intellij.psi.JavaElementVisitor;
//import com.intellij.psi.PsiElementVisitor;
//import com.intellij.psi.PsiField;
//import org.jetbrains.annotations.NotNull;
//
//public class JavaInspection extends AbstractBaseJavaLocalInspectionTool {
//    @Override
//    public PsiElementVisitor buildVisitor(@NotNull final ProblemsHolder holder, final boolean isOnTheFly){
//        JavaElementVisitor javaElementVisitor = new JavaElementVisitor() {
//            @Override
//            public void visitField(PsiField field) {
//                super.visitField(field);
//                if (field.getName().equals("name")){
//                    holder.registerProblem(field,"命名非法");
//                }
//            }
//        };
//        return javaElementVisitor;
//    }
//}
