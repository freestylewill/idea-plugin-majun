//package com.majun.action.inspection;
//
//import com.intellij.codeInspection.AbstractBaseUastLocalInspectionTool;
//import com.intellij.codeInspection.ProblemHighlightType;
//import com.intellij.codeInspection.ProblemsHolder;
//import com.intellij.psi.*;
//import com.intellij.psi.util.PsiTreeUtil;
//import com.majun.action.utils.MyFieldUtils;
//
//public class MyAbstractBaseUastLocalInspectionTool extends AbstractBaseUastLocalInspectionTool {
//    String annotaionName = "com.wangxb.annotation.AddField";
//
//    @Override
//    public PsiElementVisitor buildVisitor(ProblemsHolder holder, boolean isOnTheFly) {
//        return new JavaElementVisitor() {
//            @Override
//            public void visitAssignmentExpression(PsiAssignmentExpression expression) {
//                super.visitAssignmentExpression(expression);
//                String name = expression.getText();
//
//                PsiExpression lExpression = expression.getLExpression();
//                if (lExpression instanceof PsiReferenceExpression) {
//                    PsiReferenceExpression lExpr = (PsiReferenceExpression) lExpression;
//                    PsiElement lElement = lExpr.resolve();
//                    if (!(lElement instanceof PsiField)) {
//                        return;
//                    }
//                    PsiField field = (PsiField) lElement;
//
//                    PsiClass psiClass = PsiTreeUtil.getParentOfType(field, PsiClass.class);
//                    if (psiClass != null) {
//                        PsiAnnotation psiAnnotation = MyFieldUtils.getAnnotation(psiClass, annotaionName);
//                        if (psiAnnotation == null) {
//                            return;
//                        }
//                        String fieldName = MyFieldUtils.getAnnotationValue(psiAnnotation,"fieldName");
//                        if ("".equals(fieldName) == true){
//                            return;
//                        }
//                        if (fieldName.equals(field.getName())) {
//                            holder.registerProblem(expression, "自定义属性", ProblemHighlightType.LIKE_MARKED_FOR_REMOVAL);
//                        }
//                    }
//                }
//            }
//        };
//    }
//
//}